package app.model.dao;

import app.model.entity.User;

import java.sql.SQLException;

public interface UserDAOI {
    void addUser(User user) throws SQLException, ClassNotFoundException;

    User getUser(String login) throws SQLException, ClassNotFoundException;

    void updateUserName(User user) throws SQLException, ClassNotFoundException;

    void updateUserPassword(User user) throws SQLException, ClassNotFoundException;

    void deleteUser(User user) throws ClassNotFoundException;
}
