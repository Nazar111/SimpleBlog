package app.model.dao;

import app.model.entity.Publication;
import app.model.entity.User;
import app.model.utils.ConnectionUtil;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class PublicationDAO implements PublicationDAOI {
    private Connection connection;
    private static final Logger logger = Logger.getLogger(PublicationDAO.class);

    @Override
    public void addPublication(Publication publication, User user) throws ClassNotFoundException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MILLISECOND, 0);
        connection = ConnectionUtil.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("insert into publications(title,text,published,image,date,user_id,description,title_image,id) values(?,?,?,?,?,?,?,?,?);")) {
            statement.setString(1, publication.getTitle());
            statement.setString(2, publication.getText());
            statement.setBoolean(3, publication.isPublished());
            statement.setString(4, publication.getImage());
            statement.setTimestamp(5, new Timestamp(calendar.getTime().getTime()));
            statement.setString(6, user.getId());
            statement.setString(7, publication.getDescription());
            statement.setString(8, publication.getTitleImage());
            statement.setString(9, UUID.randomUUID().toString());
            statement.execute();
            logger.info("Add Post into database");
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("SQLException in PublicationDAO");
        } finally {
            try {
                connection.close();
                logger.info("Connection close in method: addPublication");
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("SQLException in PublicationDAO method: addPublication, in finally");
            }
        }
    }

    @Override
    public Publication getLastPublication(User user) throws ClassNotFoundException {
        Publication publication = null;
        ResultSet set = null;
        connection = ConnectionUtil.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("select * from publications where " +
                "user_id in(select id from users where id=?) " +
                "and date in(select MAX(date) from publications);")) {
            publication = new Publication();
            statement.setString(1, user.getId());
            set = statement.executeQuery();
            while (set.next()) {
                publication.setId(set.getString("id"));
                publication.setTitle(set.getString("title"));
                publication.setText(set.getString("text"));
                publication.setImage(set.getString("image"));
                publication.setDatePublication(set.getTimestamp("date"));
                publication.setPublished(set.getBoolean("published"));
                publication.setUpdateDate(set.getTimestamp("update_date"));
                publication.setUser_id(set.getString("user_id"));
                publication.setTitleImage(set.getString("title_image"));
                publication.setDescription(set.getString("description"));
            }
            logger.info("Get last publication from database");
        } catch (SQLException e) {

            logger.error("SQLException in PublicationDAO" + e.getMessage());
        } finally {
            try {
                set.close();
                connection.close();
                logger.info("Connection close in method: getLastPublication");
            } catch (SQLException e) {

                logger.error("SQLException in PublicationDAO method: getLastPublication, in finally" + e.getMessage());
            }
        }
        return new Publication();
    }

    public CopyOnWriteArrayList<Publication> getAllPublications(User user) throws SQLException, ClassNotFoundException {

        Publication publication = null;
        ResultSet set = null;

        CopyOnWriteArrayList<Publication> publications = null;
        connection = ConnectionUtil.getConnection();
        DriverManager.registerDriver(new org.postgresql.Driver());
        try (PreparedStatement statement = connection.prepareStatement("select * from publications where user_id in(select id from users where id=?) ")) {

            try {
                publications = new CopyOnWriteArrayList<>();
                statement.setString(1, user.getId());
                set = statement.executeQuery();
                while (set.next()) {
                    publication = new Publication();
                    publication.setId(set.getString("id"));
                    publication.setTitle(set.getString("title"));
                    publication.setText(set.getString("text"));
                    publication.setImage(set.getString("image"));
                    publication.setDatePublication(set.getTimestamp("date"));
                    publication.setPublished(set.getBoolean("published"));
                    publication.setUpdateDate(set.getTimestamp("update_date"));
                    publication.setDescription(set.getString("description"));
                    publication.setTitleImage(set.getString("title_image"));
                    publication.setUser_id(set.getString("user_id"));
                    publications.add(publication);
                }
                logger.info("Get all posts of user from database");
            } catch (SQLException e) {

                logger.error("SQLException in PublicationDAO method: getAllPublication" + e.getMessage());
            } finally {
                try {
                    set.close();
                    connection.close();
                    logger.info("Connection close in method: getAllPublication");
                } catch (SQLException e) {

                    logger.error("SQLException in PublicationDAO method: getAllPublication, in finally" + e.getMessage());
                }

            }
        }
        return publications;
    }

    public Publication getPublicationByDate(Date date) throws ClassNotFoundException {
        Publication publication = null;
        ResultSet set = null;
        connection = ConnectionUtil.getConnection();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        try (PreparedStatement statement = connection.prepareStatement("select * from publications where date=?;")) {
            publication = new Publication();
            statement.setTimestamp(1, new Timestamp(calendar.getTime().getTime()));
            set = statement.executeQuery();
            while (set.next()) {
                publication.setId(set.getString("id"));
                publication.setTitle(set.getString("title"));
                publication.setText(set.getString("text"));
                publication.setImage(set.getString("image"));
                publication.setDatePublication(set.getTimestamp("date"));
                publication.setPublished(set.getBoolean("published"));
                publication.setUpdateDate(set.getTimestamp("update_date"));
                publication.setUser_id(set.getString("user_id"));
                logger.info("Get post by date from database");
            }
        } catch (SQLException e) {

            logger.error("SQLException in PublicationDAO method: getPublicationByDate" + e.getMessage());
        } finally {
            try {
                set.close();
                connection.close();
                logger.info("Connection close in method: getPublicationByDate");
            } catch (SQLException e) {

                logger.error("SQLException in PublicationDAO method: getPublicationByDate, in finally" + e.getMessage());
            }
        }
        return new Publication();
    }

    @Override
    public void updatePublication(Publication publication) throws ClassNotFoundException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MILLISECOND, 0);
        connection = ConnectionUtil.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("update publications set text=?,image=?,published=?,update_date=?,description=?,title=?,title_image=? where id=? ;")) {
            statement.setString(1, publication.getText());
            statement.setString(2, publication.getImage());
            statement.setBoolean(3, publication.isPublished());
            statement.setTimestamp(4, new Timestamp(calendar.getTime().getTime()));
            statement.setString(5, publication.getDescription());
            statement.setString(6, publication.getTitle());
            statement.setString(7, publication.getTitleImage());
            statement.setString(8, publication.getId());
            statement.execute();
            logger.info("Update post from database");
        } catch (SQLException e) {

            logger.error("SQLException in PublicationDAO method: updatePublication" + e.getMessage());
        } finally {
            try {
                connection.close();
                logger.info("Connection close in method: updatePublication");
            } catch (SQLException e) {

                logger.error("SQLException in PublicationDAO method: updatePublication, in finally" + e.getMessage());
            }
        }

    }

    @Override
    public void deletePublication(Publication publication) throws SQLException, ClassNotFoundException {

        DriverManager.registerDriver(new org.postgresql.Driver());
        connection = ConnectionUtil.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("delete from publications where id=? ;")) {
            statement.setString(1, publication.getId());
            statement.execute();
            logger.info("Delete post in database");
        } catch (SQLException e) {

            logger.error("SQLException in PublicationDAO method: deletePublication" + e.getMessage());
        } finally {
            connection.close();
            logger.info("Connection close in method: deletePublication, in finally ");
        }
    }

    public Publication getPublication(String id) throws SQLException, ClassNotFoundException {
        Publication publication = null;
        ResultSet set = null;

        DriverManager.registerDriver(new org.postgresql.Driver());
        connection = ConnectionUtil.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("select * from publications where id=?;")) {

            statement.setString(1, id);
            set = statement.executeQuery();
            while (set.next()) {
                publication = new Publication();
                publication.setId(set.getString("id"));
                publication.setTitle(set.getString("title"));
                publication.setText(set.getString("text"));
                publication.setImage(set.getString("image"));
                publication.setDatePublication(set.getTimestamp("date"));
                publication.setPublished(set.getBoolean("published"));
                publication.setUpdateDate(set.getTimestamp("update_date"));
                publication.setUser_id(set.getString("user_id"));
                publication.setDescription(set.getString("description"));
                publication.setTitleImage(set.getString("title_image"));
                logger.info("Get post from database");
            }
        } catch (SQLException e) {

            logger.error("SQLException in PublicationDAO method: getPublication" + e.getMessage());
        } finally {
            set.close();
            connection.close();
            logger.info("Connection close in method: getPublication");
        }
        return publication;
    }

    public CopyOnWriteArrayList<Publication> getAll() throws SQLException, ClassNotFoundException {

        Publication publication = null;
        ResultSet set = null;
        DriverManager.registerDriver(new org.postgresql.Driver());
        CopyOnWriteArrayList<Publication> publications = null;
        connection = ConnectionUtil.getConnection();

        try (PreparedStatement statement = connection.prepareStatement("select * from publications ;")) {

            try {
                publications = new CopyOnWriteArrayList<>();
                set = statement.executeQuery();
                while (set.next()) {
                    publication = new Publication();
                    publication.setId(set.getString("id"));
                    publication.setTitle(set.getString("title"));
                    publication.setText(set.getString("text"));
                    publication.setImage(set.getString("image"));
                    publication.setDatePublication(set.getTimestamp("date"));
                    publication.setPublished(set.getBoolean("published"));
                    publication.setUpdateDate(set.getTimestamp("update_date"));
                    publication.setDescription(set.getString("description"));
                    publication.setTitleImage(set.getString("title_image"));
                    publication.setUser_id(set.getString("user_id"));
                    publications.add(publication);
                }
                logger.info("Get all posts from database");
            } catch (SQLException e) {

                logger.error("SQLException in PublicationDAO method: getAll" + e.getMessage());
            } finally {
                try {
                    set.close();
                    connection.close();
                    logger.info("Connection close in method: getAll ");
                } catch (SQLException e) {

                    logger.error("SQLException in PublicationDAO method: getAll, in finally"+e.getMessage());
                }

            }
        }
        return publications;
    }
}
