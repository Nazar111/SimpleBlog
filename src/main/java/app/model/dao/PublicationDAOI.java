package app.model.dao;

import app.model.entity.Publication;
import app.model.entity.User;

import java.sql.SQLException;


public interface PublicationDAOI {
    void addPublication(Publication publication,User user) throws ClassNotFoundException, SQLException;

    Publication getLastPublication(User user) throws ClassNotFoundException;

    void updatePublication(Publication publication) throws ClassNotFoundException;

    void deletePublication(Publication publication) throws ClassNotFoundException, SQLException;


}
