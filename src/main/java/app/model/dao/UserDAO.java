package app.model.dao;

import app.model.entity.User;
import app.model.utils.ConnectionUtil;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class UserDAO implements UserDAOI {
    private Connection connection;
    private static final Logger logger = Logger.getLogger(UserDAO.class);

    @Override
    public void addUser(User user) throws ClassNotFoundException {
        try {
            connection = ConnectionUtil.getConnection();
            try (PreparedStatement statement = connection.prepareStatement("insert into users(name,login,password,id,role) values(?,?,?,?,?::role);")) {
                statement.setString(1, user.getName());
                statement.setString(2, user.getLogin());
                statement.setString(3, user.getPassword());
                statement.setString(4, UUID.randomUUID().toString());
                statement.setString(5, user.getRole().toString());
                statement.execute();
                logger.info("Add user into database");
            }
        } catch (SQLException e) {

            logger.error("SQLException in UserDAO,method: addUser" + e.getMessage());
        } finally {
            try {
                connection.close();
                logger.info("Connection close in method: addUser");
            } catch (SQLException e) {

                logger.error("SQLException in UserDAO method: addUser, in finally" + e.getMessage());
            }
        }
    }

    @Override
    public User getUser(String login) throws ClassNotFoundException {
        User user = null;
        ResultSet set = null;
        connection = ConnectionUtil.getConnection();


        try (PreparedStatement statement = connection.prepareStatement("select * from users where login=?;")) {
            user = new User();
            statement.setString(1, login);
            set = statement.executeQuery();
            while (set.next()) {
                user.setId(set.getString("id"));
                user.setLogin(set.getString("login"));
                user.setName(set.getString("name"));
                user.setPassword(set.getString("password"));
                user.setRole(User.ROLE.valueOf(set.getString("role")));
                logger.info("Get user from database");
            }
        } catch (SQLException e) {
            logger.error("SQLException in UserDAO,method: getUser" + e.getMessage());
        } finally {
            try {
                set.close();
                connection.close();
                logger.info("Connection close in method: getUser");
            } catch (SQLException e) {
                logger.error("SQLException in UserDAO method: getUser, in finally" + e.getMessage());
            }
        }
        return user;
    }

    public User getUserById(String id) throws ClassNotFoundException {
        User user = null;
        ResultSet set = null;
        connection = ConnectionUtil.getConnection();


        try (PreparedStatement statement = connection.prepareStatement("select * from users where id=?;")) {
            user = new User();
            statement.setString(1, id);
            set = statement.executeQuery();
            while (set.next()) {
                user.setId(set.getString("id"));
                user.setLogin(set.getString("login"));
                user.setName(set.getString("name"));
                user.setPassword(set.getString("password"));
                user.setRole(User.ROLE.valueOf(set.getString("role")));
                logger.info("Get user by id from database");
            }
        } catch (SQLException e) {

            logger.error("SQLException in UserDAO method: getUserById" + e.getMessage());
        } finally {
            try {
                set.close();
                connection.close();
                logger.info("Connection close in method: getUserById");
            } catch (SQLException e) {

                logger.error("SQLException in UserDAO method: getUserById, in finally"+e.getMessage());
            }
        }

        return user;
    }

    @Override
    public void updateUserName(User user) throws ClassNotFoundException {

        connection = ConnectionUtil.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("update users set name=? where login=? ;")) {
            statement.setString(1, user.getName());
            statement.setString(2, user.getLogin());
            statement.execute();
            logger.info("Update user name in database");
        } catch (SQLException e) {
            logger.error("SQLException in UserDAO method: updateUserName"+e.getMessage());
        } finally {
            try {
                connection.close();
                logger.info("Connection close in method: updateUserName");
            } catch (SQLException e) {

                logger.error("SQLException in UserDAO method: updateUserName, in finally"+e.getMessage());
            }
        }
    }

    @Override
    public void updateUserPassword(User user) throws ClassNotFoundException {

        connection = ConnectionUtil.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("update users set password=? where login=?;")) {
            statement.setString(1, user.getPassword());
            statement.setString(2, user.getLogin());
            statement.execute();
            logger.info("Update user password in database");
        } catch (SQLException e) {
            logger.error("SQLException in UserDAO method: updateUserPassword"+e.getMessage());
        } finally {
            try {
                connection.close();
                logger.info("Connection close in method: updateUserPassword");
            } catch (SQLException e) {
                logger.error("SQLException in UserDAO method: updateUserPassword, in finally"+e.getMessage());
            }
        }
    }

    @Override
    public void deleteUser(User user) throws ClassNotFoundException {
        connection = ConnectionUtil.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("delete from users where login=? and password=?;")) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.execute();
        } catch (SQLException e) {
            logger.error("SQLException in UserDAO method: deleteUser"+e.getMessage());
        } finally {
            try {
                connection.close();
                logger.info("Connection close in method: deleteUser");
            } catch (SQLException e) {
                logger.error("SQLException in UserDAO method: deleteUser, in finally"+e.getMessage());
            }
        }

    }

    public boolean isExistUser(String login) throws ClassNotFoundException {
        User user = null;
        ResultSet set = null;
        connection = ConnectionUtil.getConnection();

        try (PreparedStatement statement = connection.prepareStatement("select * from users where login=?;")) {
            user = new User();
            statement.setString(1, login);
            set = statement.executeQuery();
            return set.next();
        } catch (SQLException e) {
            return false;
        } finally {
            try {
               set.close();
                connection.close();
                logger.info("Connection close in method: isExistUser");
            } catch (SQLException e) {
                logger.error("SQLException in UserDAO method: isExistUser, in finally"+e.getMessage());
            }
        }

    }

}
