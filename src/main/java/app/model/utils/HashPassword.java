package app.model.utils;

import app.model.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

public class HashPassword {
    public static String doHash(String password) {
        return DigestUtils.md5Hex(password);
    }

    public static boolean passwordCheck(String password, User user) {
        return user.getPassword().equals(doHash(password));
    }
}
