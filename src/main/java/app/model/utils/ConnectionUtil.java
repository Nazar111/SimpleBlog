package app.model.utils;


import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionUtil {
    private static final Logger logger = Logger.getLogger(ConnectionUtil.class);

    private ConnectionUtil() {
    }

    public static synchronized Connection getConnection() throws ClassNotFoundException {

        Class.forName("org.postgresql.Driver");
        Properties properties = new Properties();
        Connection connection = null;

        try (FileInputStream fis = new FileInputStream(ConnectionUtil.class.getClassLoader().getResource("dataConfig.properties").getPath())) {
            properties.load(fis);
            logger.info("Properties was loaded in ConnectionUtil");
            connection = DriverManager.getConnection(properties.getProperty("host"), properties.getProperty("login"), properties.getProperty("password"));
            logger.info("Get connection in ConnectionUtil");
        } catch (SQLException e) {
            logger.error("SQLException in ConnectionUtil"+e.getMessage());
        } catch (FileNotFoundException e) {

            logger.error("FileNotFoundException in ConnectionUtil"+e.getMessage());
        } catch (IOException e) {

            logger.error("IOException in ConnectionUtil"+e.getMessage());
        }

        return connection;

    }


}
