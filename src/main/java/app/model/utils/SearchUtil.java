package app.model.utils;


import app.model.dao.PublicationDAO;
import app.model.entity.Publication;

import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.HashSet;


public class SearchUtil {
    private static final Logger logger = Logger.getLogger(SearchUtil.class);
    private static PublicationDAO pDAO = null;

    public static HashSet<Publication> searchPosts(String regg) throws SQLException {
        HashSet<Publication> set = new HashSet<>();
        pDAO = new PublicationDAO();
        try {
            for (Publication p : pDAO.getAll()) {

                if (p.getTitle().toLowerCase().contains(regg.toLowerCase()) ||
                        p.getText().toLowerCase().contains(regg.toLowerCase()) ||
                        p.getDescription().toLowerCase().contains(regg.toLowerCase())) {
                    set.add(p);
                }
            }
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }

        return set;
    }

}
