package app.model.entity;


import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;


public class Publication implements Comparable {
    private String id;
    @NotNull(message = "The article should have a title")
    @Max(150)
    private String title;
    private String text;
    private String image;
    private boolean published;
    private Date datePublication;
    private Date updateDate;
    private String user_id;
    @Max(300)
    private String description;
    @NotNull
    private String titleImage;

    public Publication() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getTitleImage() {
        return titleImage;
    }

    public void setTitleImage(String titleImage) {
        this.titleImage = titleImage;
    }

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    @NotNull
    public Date getDatePublication() {

        return datePublication;
    }


    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;

    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @NotNull
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSimpleDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM Y", Locale.ENGLISH);
        if (updateDate == null) {
            return sdf.format(datePublication);
        } else return sdf.format(updateDate);
    }

    @Override
    public String toString() {
        return "Publication{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", image='" + image + '\'' +
                ", published=" + published +
                ", datePublication=" + datePublication +
                ", updateDate=" + updateDate +
                ", user_id=" + user_id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publication that = (Publication) o;
        return id == that.id &&
                published == that.published &&
                user_id == that.user_id &&
                Objects.equals(title, that.title) &&
                Objects.equals(text, that.text) &&
                Objects.equals(image, that.image) &&
                Objects.equals(datePublication, that.datePublication) &&
                Objects.equals(updateDate, that.updateDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, title, text, image, published, datePublication, updateDate, user_id);
    }

    @Override
    public int compareTo(Object o) {
        Date last = null;
        Date lastThis = null;
        Publication publ = (Publication) o;
        last = publ.getUpdateDate() != null ? publ.getUpdateDate() : publ.getDatePublication();
        lastThis = this.getUpdateDate() != null ? this.getUpdateDate() : this.getDatePublication();
        if (lastThis.getTime() != last.getTime())
            return lastThis.getTime() > last.getTime() ? -1 : 1;
        else return 0;
    }
}
