package app.model.entity;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Objects;

public class User {
    @NotNull
    private String id;
    @NotNull(message = "The name must be entered")
    @Pattern(regexp = "^[A-Z]+[a-z]+$", message = "Incorrect name")
    private String name;
    @NotNull(message = "The login must be entered")
    private String login;
    @Pattern(regexp = "^[A-Za-z0-9_-]{6,}$", message = "Incorrect password")
    @NotNull(message = "The password must be entered")
    private String password;
    @NotNull
    private ROLE role;

    public User() {
    }


    public ROLE getRole() {
        return role;
    }

    public void setRole(ROLE role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(name, user.name) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, login, password);
    }

    public enum ROLE {
        USER, ADMIN
    }
}
