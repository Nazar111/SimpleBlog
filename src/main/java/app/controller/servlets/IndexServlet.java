package app.controller.servlets;

import app.model.dao.PublicationDAO;
import app.model.entity.Publication;
import org.apache.log4j.Logger;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;

@WebServlet(urlPatterns = "")
public class IndexServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(IndexServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            PublicationDAO dao = new PublicationDAO();
            CopyOnWriteArrayList<Publication> publications = dao.getAll();
            Collections.sort(publications);
            req.setAttribute("listAll", publications);
            req.getRequestDispatcher("index.jsp").forward(req, resp);
            logger.info("First page was loaded");
        } catch (SQLException e) {
            logger.error("SQLException in IndexServlet:Possible problems with the connection"+e.getMessage());
        } catch (ClassNotFoundException e) {
          logger.error(e.getMessage());
        }

    }
}

