package app.controller.servlets;


import app.model.dao.PublicationDAO;
import app.model.dao.UserDAO;
import app.model.entity.Publication;
import app.model.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;

@MultipartConfig
@WebServlet(urlPatterns = "/updatePost")
public class UpdatePostServlet extends HttpServlet {
    private static String postId;
    private static final Logger logger = Logger.getLogger(UpdatePostServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        postId = req.getParameter("id");
        PublicationDAO dao = new PublicationDAO();

        try {
            Publication publication = dao.getPublication(postId);
            req.setAttribute("title", publication.getTitle());
            req.setAttribute("titleImage", publication.getTitleImage());
            req.setAttribute("description", publication.getDescription());
            req.setAttribute("text", publication.getText());
            req.setAttribute("image", publication.getImage());
            req.setAttribute("id", postId);
            req.getRequestDispatcher("/view/updatePost.jsp").forward(req, resp);
            logger.info("The old data in UpdatePostServlet was loaded");
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("The old data in UpdatePostServlet was not loaded, SQLException");
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

        final String postId = req.getParameter("id");
        PublicationDAO dao = new PublicationDAO();
        HttpSession session = req.getSession();
        File uploads = new File(req.getServletContext().getRealPath("/content"));
        try {
            Publication publication = dao.getPublication(postId);


            final String login = (String) session.getAttribute("login");
            final User user = new UserDAO().getUser(login);
            final String title = req.getParameter("title");
            final String titleImage = req.getParameter("titleImage");
            final String description = req.getParameter("description");
            final String image = req.getParameter("image");
            final String text = req.getParameter("text");
            final boolean isDash = req.getParameter("dash") == null;
            Part filePart = req.getPart("image");
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            if (fileName.equals("")) {
                publication.setImage("");
            } else {
                String preffix = fileName.split("[.]")[0];
                String suffix = "." + fileName.split("[.]")[1];
                File file = new File(uploads, preffix + suffix);

                try (InputStream input = filePart.getInputStream()) {
                    Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
                publication.setImage(file.getName());
            } Part filePartTI = req.getPart("titleImage");
            String fileNameTI = Paths.get(filePartTI.getSubmittedFileName()).getFileName().toString();
            if (fileNameTI.equals("")) {
                publication.setTitleImage("image/pic_blog_post_1_1.jpg");
            } else {
                String preffixTI = fileNameTI.split("[.]")[0];
                String suffixTI = "." + fileNameTI.split("[.]")[1];
                File fileTI = new File(uploads, preffixTI + suffixTI);
                try (InputStream input = filePartTI.getInputStream()) {
                    Files.copy(input, fileTI.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
                publication.setTitleImage(fileTI.getName()); }






            publication.setUser_id(user.getId());
            publication.setText(text);
            publication.setTitle(title);

            publication.setDescription(description);

            publication.setPublished(isDash);
            dao.updatePublication(publication);
            req.getRequestDispatcher("/userPage.jsp").forward(req, resp);
            logger.info("The post was updated");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.error("ClassNotFoundException in UpdatePostServlet");
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("IOException in UpdatePostServlet");
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("SQLException in UpdatePostServlet: Possible problems with the connection");
        } catch (ServletException e) {
            e.printStackTrace();
            logger.error("ServletException in UpdatePostServlet");
        }

    }


}
