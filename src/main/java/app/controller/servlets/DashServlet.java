package app.controller.servlets;

import app.model.dao.PublicationDAO;
import app.model.dao.UserDAO;
import app.model.entity.Publication;
import app.model.entity.User;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;

@WebServlet(urlPatterns = "/dash")
public class DashServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(DashServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            final String name = (String) req.getSession().getAttribute("name");
            final User user = new UserDAO().getUser((String) req.getSession().getAttribute("login"));
            req.setAttribute("name", name);
            PublicationDAO dao = new PublicationDAO();
            CopyOnWriteArrayList<Publication> publications = dao.getAllPublications(user);
            Collections.sort(publications);
            req.setAttribute("list", publications);
            req.getRequestDispatcher("/view/dash.jsp").forward(req, resp);
            logger.info("the draft was loaded");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.error("ClassNotFoundException in DashServlet");
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("SQLExeption in DashServlet");
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        PublicationDAO dao = new PublicationDAO();

        try {
            Publication publication = dao.getPublication(id);
            publication.setPublished(true);
            dao.updatePublication(publication);
            req.getRequestDispatcher("/userPage.jsp").forward(req, resp);
            logger.info("The post was moved from the draft");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.error("ClassNotFoundException in DashServlet");
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("SQLException e in DashServlet :Possible problems with the connection");
        }
    }
}

