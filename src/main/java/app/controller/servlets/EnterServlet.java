package app.controller.servlets;

import app.model.dao.UserDAO;
import app.model.utils.HashPassword;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(urlPatterns = "/enter")
public class EnterServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(EnterServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect(req.getContextPath()+ "/userPage.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String login = req.getParameter("login");
        final String password = req.getParameter("password");
        HttpSession session = req.getSession();
        UserDAO dao = new UserDAO();
        try {
            if (dao.isExistUser(login) && HashPassword.passwordCheck(password,dao.getUser(login))) {
                session.setAttribute("name", new UserDAO().getUser(login).getName());
                session.setAttribute("login", login);
//                session.setAttribute("password", password);
                logger.info("User " + login + " log in");
                doGet(req, resp);
            } else {
                req.setAttribute("message", "Invalid password or login!");
                req.getRequestDispatcher("/").forward(req, resp);
                logger.info("The user could not log in");
            }
        } catch (ClassNotFoundException e) {

            logger.error("ClassNotFoundException in EnterServlet"+e.getMessage());
        }
    }
}
