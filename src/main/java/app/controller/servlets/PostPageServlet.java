package app.controller.servlets;

import app.model.dao.PublicationDAO;
import app.model.dao.UserDAO;
import app.model.entity.Publication;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(urlPatterns = "/postPage")
public class PostPageServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(PostPageServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession session = req.getSession();
            final String name = (String) session.getAttribute("name");
            final String postId = req.getParameter("id");
            boolean logged = false;
            if (name != null) {
                logged = true;
            }
            PublicationDAO dao = new PublicationDAO();
            Publication publication = dao.getPublication(postId);
            req.setAttribute("name", new UserDAO().getUserById(dao.getPublication(postId).getUser_id()).getName());
            req.setAttribute("title", publication.getTitle());
            req.setAttribute("titleImage", publication.getTitleImage());
            req.setAttribute("description", publication.getDescription());
            req.setAttribute("text", publication.getText());
            req.setAttribute("image", publication.getImage());
            req.setAttribute("date", publication.getSimpleDate());
            req.setAttribute("postId", postId);
            req.setAttribute("published", publication.isPublished());
            req.setAttribute("logged", logged);
            req.getRequestDispatcher("/view/postPage.jsp").forward(req, resp);
            logger.info("Post " + postId + " was loaded");
        } catch (SQLException e) {

            logger.error("SQLException in PostPageServlet: Possible problems with the connection"+e.getMessage());
        } catch (ClassNotFoundException e) {

            logger.error("ClassNotFoundException in PostPageServlet"+e.getMessage());
        }

    }
}

