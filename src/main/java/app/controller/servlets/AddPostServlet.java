package app.controller.servlets;

import app.model.dao.PublicationDAO;
import app.model.dao.UserDAO;
import app.model.entity.Publication;
import app.model.entity.User;


import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


@MultipartConfig
@WebServlet(urlPatterns = "/addPost")
public class AddPostServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(AddPostServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        PublicationDAO dao = new PublicationDAO();
        File uploads = new File(req.getServletContext().getRealPath("/content"));

        req.setCharacterEncoding("UTF8");
        try {
            final String login = (String) session.getAttribute("login");
            final User user = new UserDAO().getUser(login);
            final String title = req.getParameter("title");
            final String description = req.getParameter("description");
            final String text = req.getParameter("text");
            final boolean isDash = req.getParameter("dash") == null;
            Publication publication = new Publication();

            Part filePart = req.getPart("image");
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            if (fileName.equals("")) {
                publication.setImage("");
            } else {
                String preffix = fileName.split("[.]")[0];
                String suffix = "." + fileName.split("[.]")[1];
                File file = new File(uploads, preffix + suffix);

                try (InputStream input = filePart.getInputStream()) {
                    Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
                publication.setImage(file.getName());
            }


            Part filePartTI = req.getPart("titleImage");
            String fileNameTI = Paths.get(filePartTI.getSubmittedFileName()).getFileName().toString();
            if (fileNameTI.equals("")) {
                publication.setTitleImage("image/pic_blog_post_1_1.jpg");
            } else {
                String preffixTI = fileNameTI.split("[.]")[0];
                String suffixTI = "." + fileNameTI.split("[.]")[1];
                File fileTI = new File(uploads, preffixTI + suffixTI);
                try (InputStream input = filePartTI.getInputStream()) {
                    Files.copy(input, fileTI.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
                publication.setTitleImage(fileTI.getName());

                publication.setUser_id(user.getId());
                publication.setText(text);
                publication.setTitle(title);
                publication.setDescription(description);
                publication.setPublished(isDash);
                dao.addPublication(publication, user);
            }


           req.getRequestDispatcher("/userPage.jsp").forward(req,resp);
            logger.info("User"+login+" was added post - "+title);
        } catch (ClassNotFoundException e) {
            logger.error("ClassNotFoundException in AddPostServlet");
         logger.error(e.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/view/addPostForm.jsp").forward(req,resp);
    }
}
