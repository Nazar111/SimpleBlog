package app.controller.servlets;

import app.model.dao.PublicationDAO;
import app.model.entity.Publication;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;

@WebServlet(urlPatterns = "/homePage")
public class HomePageServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(HomePageServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            final String name = (String) req.getSession().getAttribute("name");
            PublicationDAO dao = new PublicationDAO();
            CopyOnWriteArrayList<Publication> publications = dao.getAll();
            Collections.sort(publications);
            req.setAttribute("list", publications);
            req.setAttribute("name", name);
            req.getRequestDispatcher("/view/homePage.jsp").forward(req, resp);
            logger.info("Home page was loaded");
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("SQLException in HomePageServlet:Possible problems with the connection");
        } catch (ClassNotFoundException e) {
           logger.error(e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
