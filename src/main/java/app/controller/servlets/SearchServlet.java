package app.controller.servlets;

import app.model.utils.SearchUtil;
import app.model.dao.UserDAO;
import app.model.entity.Publication;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

@WebServlet(urlPatterns = "/search")
public class SearchServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        final String search = req.getParameter("search");
        final String name = (String) session.getAttribute("name");
        boolean logged = false;
        if (name != null) {
            logged = true;
        }
        try {
            HashSet<Publication> set = SearchUtil.searchPosts(search);
            ArrayList<String> names = new ArrayList<>();
            for (Publication p : set) {
                names.add(new UserDAO().getUserById(p.getUser_id()).getName());
            }
            req.setAttribute("result",set.size());
            req.setAttribute("set", set);
            req.setAttribute("names",names);
            req.setAttribute("logged", logged);
            req.getRequestDispatcher("/view/searchPage.jsp").forward(req, resp);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
