package app.controller.servlets;

import app.model.dao.UserDAO;
import app.model.entity.User;
import app.model.utils.HashPassword;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

@WebServlet(urlPatterns = "/registration")
public class RegistrationServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(RegistrationServlet.class);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/view/registration.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF8");
        final String login = req.getParameter("login");
        final String name = req.getParameter("name");
        final String password = req.getParameter("password");
        final String isAdmin = req.getParameter("Role");

        User user = new User();
        user.setLogin(login);
        user.setName(name);
        user.setPassword(HashPassword.doHash(password));
        user.setRole(isAdmin != null ? User.ROLE.ADMIN : User.ROLE.USER);


        UserDAO dao = new UserDAO();
        HttpSession session = req.getSession();
        try {
            if (!(new UserDAO().isExistUser(login))) {
                dao.addUser(user);
                session.setAttribute("name", name);
                session.setAttribute("login", login);
//                session.setAttribute("password", password);
                session.setAttribute("role", user.getRole());
                req.getRequestDispatcher("/userPage.jsp").forward(req, resp);
                logger.info("User " + login + " was registered");
            } else {
                req.setAttribute("message", "User alredy exist!");
                req.getRequestDispatcher("/view/registration.jsp").forward(req, resp);
                logger.info("User " + login + " was not registered");
            }

        } catch (ClassNotFoundException e) {
            logger.error("ClassNotFoundException in RegistrationServlet"+e.getMessage());
        }


    }


}

