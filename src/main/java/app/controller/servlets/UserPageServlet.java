package app.controller.servlets;

import app.model.dao.PublicationDAO;
import app.model.dao.UserDAO;
import app.model.entity.Publication;
import app.model.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;

@WebServlet(urlPatterns = "/userPage.jsp")
public class UserPageServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(UserPageServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            final String name = (String) req.getSession().getAttribute("name");
            final User user = new UserDAO().getUser((String) req.getSession().getAttribute("login"));
            req.setAttribute("name", name);
            PublicationDAO dao = new PublicationDAO();
            CopyOnWriteArrayList<Publication> publications = dao.getAllPublications(user);
            Collections.sort(publications);
            req.setAttribute("list", publications);
            req.getRequestDispatcher("/view/userPage.jsp").forward(req, resp);
            logger.info("The user page was loaded");
        } catch (ClassNotFoundException e) {

            logger.error("ClassNotFoundException in UserPageServlet"+e.getMessage());
        } catch (SQLException e) {

            logger.error("SQLException in UserPageServlet: Possible problems with the connection"+e.getMessage());
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}

