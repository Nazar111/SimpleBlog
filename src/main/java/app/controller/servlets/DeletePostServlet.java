package app.controller.servlets;

import app.model.dao.PublicationDAO;

import app.model.entity.Publication;
import org.apache.log4j.Logger;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/deletePost")
public class DeletePostServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(DeletePostServlet.class);
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        try {
            PublicationDAO dao = new PublicationDAO();
            Publication publication = dao.getPublication(id);
            dao.deletePublication(publication);
            if (req.getParameter("isDash") != null) {
                resp.sendRedirect("/dash");
            } else {
                req.getRequestDispatcher("/userPage.jsp").forward(req, resp);
            }
       logger.info("The post"+id+"was deleted");
        } catch (SQLException e) {
            e.printStackTrace();
        logger.error("SQLException in DeletePostServlet:Possible problems with the connection");
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }


}
