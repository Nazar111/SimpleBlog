package app.controller.servlets;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = "/logOut")
public class LogOutServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(LogOutServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        final HttpSession session = req.getSession();

        try {
            session.removeAttribute("password");
            session.removeAttribute("login");
            session.removeAttribute("name");

            resp.sendRedirect(req.getContextPath()+"/");
            logger.info("The user was log out");
        } catch (IOException e) {

            logger.error("IOException in LogOutServlet"+e.getMessage());
        }
    }
}
