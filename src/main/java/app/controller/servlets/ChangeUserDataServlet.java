package app.controller.servlets;

import app.model.dao.UserDAO;
import app.model.entity.User;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(urlPatterns = "/changeUserData")
public class ChangeUserDataServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ChangeUserDataServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String login = (String) req.getSession().getAttribute("login");
        final String newName = req.getParameter("newName");
        final String newPassword = req.getParameter("newPassword");
        UserDAO dao = new UserDAO();
        try {
            User user = dao.getUser(login);
            user.setName(newName);
            user.setPassword(newPassword);
            dao.updateUserName(user);
            dao.updateUserPassword(user);
            req.getSession().setAttribute("name", newName);
            req.getSession().setAttribute("password", newPassword);

            doGet(req, resp);
            logger.info("The user "+login+" has edited his profile");
        } catch (ClassNotFoundException e) {
            logger.error("ClassNotFoundException in ChangeUserDataServlet");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String name = (String) req.getSession().getAttribute("name");
        req.setAttribute("name", name);
        req.getRequestDispatcher("view/homePage.jsp").forward(req, resp);
    }
}
