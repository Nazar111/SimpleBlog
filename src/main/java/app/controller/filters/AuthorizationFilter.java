package app.controller.filters;


import app.model.entity.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.Validator;

import java.io.IOException;
import java.util.Set;

@WebFilter(urlPatterns = "/authorization")
public class AuthorizationFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpServletRequest req = (HttpServletRequest) request;

        final String name = request.getParameter("name");
        final String login = request.getParameter("login");
        final String password = request.getParameter("password");
        try (ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory()) {
            Validator validator = validatorFactory.getValidator();
            Set<ConstraintViolation<User>> violationName = validator.validateValue(User.class, "name", name);
            for (ConstraintViolation v : violationName) System.out.println(v.getMessage());
            Set<ConstraintViolation<User>> violationLogin = validator.validateValue(User.class, "login", login);
            Set<ConstraintViolation<User>> violationPassword = validator.validateValue(User.class, "password", password);
            if (violationName.size() > 0 || violationLogin.size() > 0 || violationPassword.size() > 0) {
                if (violationName.size() > 0) {
                    req.setAttribute("invalidName", "in");
                }
                if (!violationLogin.isEmpty()) {
                    req.setAttribute("invalidLogin", "in");
                }
                if (!violationPassword.isEmpty()) {
                    req.setAttribute("invalidPassword", "in");
                }
                req.setAttribute("violationName", violationName);
                req.setAttribute("violationLogin", violationLogin);
                req.setAttribute("violationPassword", violationPassword);
                req.getRequestDispatcher("/view/registration.jsp").forward(req, resp);
            } else {
                req.getRequestDispatcher("/registration").forward(req, resp);
            }


        }
    }

    @Override
    public void destroy() {

    }
}