package app.controller.filters;

import app.model.entity.Publication;

import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.util.Set;
@MultipartConfig
//@WebFilter(urlPatterns = "/addPost")
public class PostValidationFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpServletRequest req = (HttpServletRequest) request;
        final String title = req.getParameter("title");
        final String description = req.getParameter("description");
        try (ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory()) {
            Validator validator = validatorFactory.getValidator();
            Set<ConstraintViolation<Publication>> title1 = validator.validateValue(Publication.class, "title", title);
            Set<ConstraintViolation<Publication>> description1 = validator.validateValue(Publication.class, "description", description);
            if (title1.size() > 0 || description1.size() > 0) {
                if (title1.size() > 0) {
                    req.setAttribute("invalidTitle", "in");
                    req.setAttribute("titleError", title1);
                }
                if (description1.size() > 0) {
                    req.setAttribute("descriptionError", description1);
                    req.setAttribute("invalidDesc", "in");
                }
                if (req.getServletPath().equals("/addPost")) {
                    req.getRequestDispatcher("/view/addPostForm.jsp").forward(req, resp);
                } else {
                    req.getRequestDispatcher("/view/updatePost.jsp").forward(req, resp);
                }


            } else {
//                req.getRequestDispatcher("/userPage.jsp").forward(req, resp);
            }

        }

    }

    @Override
    public void destroy() {

    }
}
