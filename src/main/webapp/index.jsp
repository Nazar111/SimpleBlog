<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SimpleBlog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="layout/plugins/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="layout/style.css" type="text/css">
    <script type="text/javascript" src="layout/js/jquery.js"></script>
    <script type="text/javascript" src="layout/js/plugins.js"></script>
    <script type="text/javascript" src="layout/js/main.js"></script>
</head>
<body>
<div>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
        <a class="btn btn-dark" href="${pageContext.request.contextPath}/registration" id="Registration"
           name="registration">Sign Up</a>

        <details class="btn btn-dark btn-sm" style="outline: none ">
            <summary>Log in</summary>

            <form id="login" method="post" action="${pageContext.request.contextPath}/enter">
                <input type="text" name="login" placeholder="Enter your login">
                <input type="password" name="password" placeholder="Enter your password"/>
                <input type="submit" value="Enter" class="btn btn-success btn-sm"/>
            </form>

            <p style="color: red"><c:out value="${message}"/></p>

        </details>
        <form style="margin-left: 61rem" class="form-inline" method="post"
              action="${pageContext.request.contextPath}/search">
            <input type="search" class="form-control mr-sm-2" name="search">
            <input type="image" src="${pageContext.request.contextPath}/layout/images/icon_search_1.png"/>
        </form>
    </nav>


</div>
<div id="logo" style=""><p><img src="image/logo.png"></p>
</div>
<%--<div id="category"><p >Trave Music Image History Sport</p></div>--%>
<div id="content" class="">
    <div class="inner">
        <div class="block_slider_type_1 general_not_loaded">
            <div id="slider" class="slider flexslider">
                <ul class="slides">
                    <c:forEach var="pub1" items="${listAll}">
                        <c:if test="${pub1.published==true}">
                            <li>
                                <div style="max-width: 60rem;max-height: 30rem;display: block;background-image: url(${pageContext.request.contextPath}/layout/images/back_roll.jpg); ">
                                    <img src="content/${pub1.titleImage}" alt=""
                                         style="height:30rem;; width: auto;margin-left:auto;margin-right: auto;display: block;"/>
                                    <div class="animated_item text_1_1" data-animation-show="fadeInUp"
                                         data-animation-hide="fadeOutDown"></div>
                                    <div class="animated_item text_1_2" data-animation-show="fadeInUp"
                                         data-animation-hide="fadeOutDown"><c:out value="${pub1.title}"/></div>
                                    <div class="animated_item text_1_3" data-animation-show="fadeInUp"
                                         data-animation-hide="fadeOutDown"><a
                                            href="${pageContext.request.contextPath}/postPage?id=${pub1.id}"
                                            class="general_button_type_1">Read More</a>
                                    </div>
                                </div>
                            </li>

                        </c:if>
                    </c:forEach>
                </ul>

            </div>

            <script type="text/javascript">
                jQuery(function () {
                    init_slider_1('#slider');
                });
            </script>
        </div>
    </div>
</div>

<div class="block_general_title_1 w_margin_1">
    <h1>Latest Posts</h1>
</div>

<div class="block_posts type_1 type_sort general_not_loaded">

    <div class="posts" style="margin-left: 5.5rem;" id="con">

        <c:forEach var="pub1" items="${listAll}">
            <c:if test="${pub1.published==true}">


                <article class="post_type_1">
                    <div class="feature">
                        <div class="image">
                            <c:if test="${pub1.titleImage!=''}">

                                <a href="${pageContext.request.contextPath}/postPage?id=${pub1.id}">
                                    <img src="content/${pub1.titleImage}">
                                    <span class="hover"> <h3><p
                                            style="color: white;background: rgba(0, 0, 0, 0.7); padding: 10px;"><c:out
                                            value="${pub1.title}"/><br/></p></h3></span></a>

                            </c:if>
                            <script>
                                $(function () {
                                    $("h3").wrapInner("<span>");
                                    $("h3 br").before("<span class='spacer'>")
                                        .after("<span class='spacer'>");
                                });</script>


                        </div>
                    </div>

                    <div class="content">
                        <div class="info">

                            <div class="date"><c:out value="${pub1.simpleDate}"/></div>
                            <div class="stats">


                            </div>
                        </div>
                        <div class="title">
                            <a href="${pageContext.request.contextPath}/postPage?id=${pub1.id}"><c:out
                                    value="${pub1.description}"/></a>
                        </div>
                    </div>
                </article>
            </c:if>
        </c:forEach>

    </div>
    <div class="controls">
        <a href="#" id="button_load_more" data-target=".block_posts.type_sort .posts" class="load_more_1"><span>Load more posts</span></a>
    </div>
</div>


</body>
</html>

