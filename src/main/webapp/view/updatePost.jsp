<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SimpleBlog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style">
</head>
<body>
<div>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a href="${pageContext.request.contextPath}/userPage.jsp" class="btn btn-dark">My page</a>
    </nav>
    <div id="logo" style=""><a href="${pageContext.request.contextPath}/homePage"><img src="image/logo.png"></a></div>
    <div class="container h-100" style="background-image: url(${pageContext.request.contextPath}/layout/images/fon_reg.jpg);">
        <div class="row h-100 justify-content-center align-items-center">
            <form id="update-post" method="post" action="${pageContext.request.contextPath}/updatePost" enctype="multipart/form-data" class="form-group">
                <input type="text" name="title" class="form-control is-${invalidTitle}valid"
                       value="<c:out value="${requestScope.title}"/>">
                <c:forEach var="titleError" items="${titleError}"><p style="color: red"><c:out
                        value="${titleError.message}"/></p>
                </c:forEach>
                <input type="file" name="titleImage" value="<c:out value="${requestScope.titleImage}"/>" class="form-control"/>
                <textarea id="description" class="form-control is-${invalidDesc}valid" name="description" cols="20"
                          rows="7"><c:out
                        value="${requestScope.description}"/></textarea>
                <c:forEach var="descriptionError" items="${descriptionError}"><p style="color: red">
                    <c:out value="${descriptionError.message}"/></p></c:forEach>
                <textarea id="text" class="form-control is-valid" name="text" cols="50" rows="15"><c:out value="${requestScope.text}" /></textarea>
                <input type="file" name="image" class="form-control" value="<c:out value="${requestScope.image}"/>"/>
                <input type="hidden" name="id" class="form-control is-valid" value="${requestScope.id}"/>
                <input type="submit"  value="Update" class="btn btn-success btn-sm"/>
                <input type="submit" value="Add to dash" name="dash" class="btn btn-success btn-sm"/>


            </form>
        </div>
    </div>
</div>
</body>
</html>
