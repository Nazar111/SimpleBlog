<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SimpleBlog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="layout/plugins/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="layout/style.css" type="text/css">
    <script type="text/javascript" src="layout/js/jquery.js"></script>
    <script type="text/javascript" src="layout/js/plugins.js"></script>
    <script type="text/javascript" src="layout/js/main.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div>
        <%--HomePage--%>
        <a href="${pageContext.request.contextPath}/homePage" class="btn btn-dark">Home page</a>

        <a id="user-name-label" href="userPage.jsp" class="btn btn-dark  "><c:out value="${requestScope.name}"/></a>

        <details class="btn btn-dark btn-sm" id="change-details">
            <summary>Change Data</summary>
            <form id="change-user-data" method="post" action="${pageContext.request.contextPath}/changeUserData">
                <input type="text" name="newName" placeholder="Enter new name">
                <input type="password" name="newPassword" placeholder="Enter new password">
                <input type="submit" value="Change" class="btn btn-success btn-sm">
            </form>
        </details>
        <a class="btn btn-dark" id="dash" href="${pageContext.request.contextPath}/dash">Dash</a>
        <a href="${pageContext.request.contextPath}/logOut" class="btn btn-dark">Log out</a>
    </div>

</nav>
<div id="logo" style=""><a href="${pageContext.request.contextPath}/homePage"><img src="image/logo.png"></a></div>
</div>


<a href="${pageContext.request.contextPath}/addPost" class="btn btn-success btn-sm">Add post</a>


<%--<div id="lastest-posts">--%>
<%--<h3><c:out value="${pub.title}"/></h3>--%>
<%--<c:if test="${pub.titleImage!=''}">--%>
<%--<img src="content/${pub.titleImage}">--%>
<%--</c:if>--%>
<%--<h4><c:out value="${pub.description}"/></h4>--%>
<%--<p><c:out value="${pub.text}"/></p>--%>
<%--<c:if test="${pub.image!=''}">--%>
<%--<img src="content/${pub.image}"/>--%>
<%--</c:if>--%>

<%--</div>--%>
<%--<form method="get" action="/updatePost">--%>
<%--<input type="hidden" name="id" value="${pub.id}"/>--%>
<%--<input type="submit" value="Edit">--%>

<%--</form>--%>
<%--<form method="post" action="/deletePost">--%>
<%--<input type="hidden" name="id" value="${pub.id}"/>--%>
<%--<input type="submit" value="Delete">--%>

<%--</form>--%>
<%--</div>--%>


<div class="block_general_title_1 w_margin_1">
    <h1>Latest Posts</h1>
</div>
<div class="block_posts type_1 type_sort general_not_loaded">
    <div class="posts" style="margin-left: 5.5rem;">
        <c:forEach var="pub" items="${list}">
            <c:if test="${pub.published==true}">


                <article class="post_type_1">
                    <div class="feature">
                        <div class="image">
                            <c:if test="${pub.titleImage!=''}">

                                    <a href="${pageContext.request.contextPath}/userPostPage?id=${pub.id}">
                                        <img src="content/${pub.titleImage}">
                                        <span class="hover"> <h3 ><p style="color: white;background: rgba(0, 0, 0, 0.7); padding: 10px;"><c:out value="${pub.title}"/><br/></p></h3></span></a>

                            </c:if>
                            <script>
                                $(function () {
                                    $("h3").wrapInner("<span>");
                                    $("h3 br").before("<span class='spacer'>")
                                        .after("<span class='spacer'>");
                                });</script>


                        </div>
                    </div>

                    <div class="content">
                        <div class="info">

                            <div class="date"><c:out value="${pub.simpleDate}"/></div>
                            <div class="stats">

                                    </form>
                                    <form class="comments" method="post" action="${pageContext.request.contextPath}/deletePost">
                                    <input type="hidden" name="id" value="${pub.id}"/>
                                    <input type="image" src="image/dump.png"style="width: 1.15rem;height: 1.15rem;">
                                    </form>

                                <form class="likes" method="get" action="${pageContext.request.contextPath}/updatePost"
                                      style="background-color: white;">
                                    <input type="hidden" name="id" value="${pub.id}"/>
                                    <input type="image" src="image/iconFix.png" style="width: 1.15rem;height: 1.15rem;">
                                </form>
                            </div>
                        </div>

                        <div class="title">
                            <a href="${pageContext.request.contextPath}/userPostPage?id=${pub.id}"><c:out value="${pub.description}"/></a>
                        </div>
                    </div>
                </article>
            </c:if>
        </c:forEach>
    </div>
    <div class="controls">
        <a href="#" id="button_load_more" data-target=".block_posts.type_sort .posts" class="load_more_1"><span>Load more posts</span></a>
    </div>
</div>


</body>
</html>
