<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SimpleBlog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
    <script type="text/javascrip t" src="layout/plugins/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="layout/style.css" type="text/css">
    <script type="text/javascript" src="layout/js/jquery.js"></script>
    <script type="text/javascript" src="layout/js/plugins.js"></script>
    <script type="text/javascript" src="layout/js/main.js"></script>
</head>
<body>

<c:if test="${published==true}">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div>
                <%--HomePage--%>
            <a href="${pageContext.request.contextPath}/homePage" class="btn btn-dark">Home page</a>
            <a id="user-name-label" href="${pageContext.request.contextPath}/userPage.jsp" class="btn btn-dark"><c:out value="${requestScope.name}"/></a>

            <details class="btn btn-dark btn-sm" id="change-details">
                <summary>Change Data</summary>
                <form id="change-user-data" method="post" action="${pageContext.request.contextPath}/changeUserData">
                    <input type="text" name="newName" placeholder="Enter new name">
                    <input type="password" name="newPassword" placeholder="Enter new password">
                    <input type="submit" value="Change" class="btn btn-success btn-sm">
                </form>
            </details>
            <a class="btn btn-dark" id="dash" href="${pageContext.request.contextPath}/dash">Dash</a>
            <a href="${pageContext.request.contextPath}/logOut" class="btn btn-dark">Log out</a>
        </div>

    </nav>

<div id="content" class="sidebar_right" style="margin-left: 16.53rem;">
    <div class="inner">

        <div class="block_general_title_2" style="margin-right:11.8rem; ">
            <h1><c:out value="${title}"/></h1>
            <h2><p><span class="author">by  <c:out value="${name}"/></span>&nbsp;&nbsp;/&nbsp;&nbsp;<span
                    class="date"> <c:out value="${date}"/> </span></p></h2>
            <div class="stats">
                </form>
                <form class="comments" method="post" action="${pageContext.request.contextPath}/deletePost">
                    <input type="hidden" name="id" value="${postId}"/>
                    <input type="image" src="image/dump.png" style="width: 1.15rem;height: 1.15rem;">
                </form>
                <form class="likes" method="get" action="${pageContext.request.contextPath}/updatePost"
                      style="background-color: white;">
                    <input type="hidden" name="id" value="${postId}"/>
                    <input type="image" src="image/iconFix.png" style="width: 1.15rem;height: 1.15rem;">
                </form>
            </div>
        </div>
        <div class="main_content" style="width: 47.2rem;">
            <div class="block_content">
                <div class="pic"><img src="content/${titleImage}" alt=""></div>
                <h3><c:out value="${description}"/></h3>

                <p><c:out value="${text}"/> <br/></p>

                <div class="pic"><img src="content/${image}" alt=""></div>

                <div class="line_1"></div>
            </div>
        </div>
    </div>
</div>
</c:if>
<c:if test="${published==false}">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div >
                <%--HomePage--%>
            <a href="${pageContext.request.contextPath}/homePage" class="btn btn-dark">Home page</a>
            <a id="user-name-label" href="${pageContext.request.contextPath}/userPage.jsp" class="btn btn-dark"><c:out value="${name}"/></a>

            <details class="btn btn-dark btn-sm" id="change-details">
                <summary>Change Data</summary>
                <form id="change-user-dataD" method="post" action="${pageContext.request.contextPath}/changeUserData">
                    <input type="text" name="newName" placeholder="Enter new name">
                    <input type="password" name="newPassword" placeholder="Enter new password">
                    <input type="submit" value="Change" class="btn btn-success btn-sm">
                </form>
            </details>
            <a class="btn btn-dark"  id="my-page" href="userPage.jsp">My page</a>
            <a href="${pageContext.request.contextPath}/logOut" class="btn btn-dark">Log out</a>
        </div>

    </nav>
    <div id="content" class="sidebar_right" style="margin-left: 16.53rem;">
        <div class="inner">

            <div class="block_general_title_2" style="margin-right: 11.8rem; ">
                <h1><c:out value="${title}"/></h1>
                <h2><p><span class="author">by  <c:out value="${name}"/></span>&nbsp;&nbsp;/&nbsp;&nbsp;<span
                        class="date"> <c:out value="${date}"/> </span></p></h2>
                <div class="stats">
                    </form>
                    <form class="comments" method="post" action="${pageContext.request.contextPath}/deletePost">
                        <input type="hidden" name="id" value="${postId}"/>
                        <input type="image" src="image/dump.png" style="width: 1.15rem;height: 1.15rem;">
                    </form>
                    <form class="likes" method="get" action="${pageContext.request.contextPath}/updatePost"
                          style="background-color: white;">
                        <input type="hidden" name="id" value="${postId}"/>
                        <input type="image" src="image/iconFix.png" style="width: 1.15rem;height: 1.15rem;">
                    </form>
                    <form class="likes" method="get" action="${pageContext.request.contextPath}/updatePost"
                          style="background-color: white;">
                        <input type="hidden" name="id" value="${postId}"/>
                        <input type="image" src="image/pub_icon.jpg" style="width: 1.15rem;height: 1.15rem;">
                    </form>
                </div>
            </div>
            <div class="main_content" style="width:  47.2rem;">
                <div class="block_content">
                    <div class="pic"><img src="content/${titleImage}" alt=""></div>
                    <h3><c:out value="${description}"/></h3>

                    <p><c:out value="${text}"/> <br/></p>

                    <div class="pic"><img src="content/${image}" alt=""></div>

                    <div class="line_1"></div>
                </div>
            </div>
        </div>
    </div>
</c:if>
    </body>
</html>
