<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SimpleBlog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="layout/plugins/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="layout/style.css" type="text/css">
    <script type="text/javascript" src="layout/js/jquery.js"></script>
    <script type="text/javascript" src="layout/js/plugins.js"></script>
    <script type="text/javascript" src="layout/js/main.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
<div >
    <%--HomePage--%>
    <a href="${pageContext.request.contextPath}/homePage" class="btn btn-dark">Home page</a>
    <a id="user-name-label" href="${pageContext.request.contextPath}/userPage.jsp" class="btn btn-dark"><c:out value="${name}"/></a>

    <details class="btn btn-dark btn-sm" id="change-details">
        <summary>Change Data</summary>
        <form id="change-user-data" method="post" action="${pageContext.request.contextPath}/changeUserData">
            <input type="text" name="newName" placeholder="Enter new name">
            <input type="password" name="newPassword" placeholder="Enter new password">
            <input type="submit" value="Change" class="btn btn-success btn-sm">
        </form>
    </details>
        <a class="btn btn-dark"  id="my-page" href="${pageContext.request.contextPath}/userPage.jsp">My page</a>
    <a href="${pageContext.request.contextPath}/logOut" class="btn btn-dark">Log out</a>
</div>

</nav>
<div id="logo" ><p><img src="image/logo.png"></p></div>

<div class="block_general_title_1 w_margin_1">
    <h1>Dash</h1>
</div>
<div class="block_posts type_1 type_sort general_not_loaded">
    <div class="posts" style="margin-left: 5.5rem;">
<c:forEach var="pub" items="${requestScope.list}">
    <c:if test="${pub.published==false}">

        <article class="post_type_1">
            <div class="feature">
                <div class="image">
                    <c:if test="${pub.titleImage!=''}">

                        <a href="${pageContext.request.contextPath}/userPostPage?id=${pub.id}">
                            <img src="content/${pub.titleImage}">
                            <span class="hover"> <h3 ><p style="color: white;background: rgba(0, 0, 0, 0.7); padding: 10px;"><c:out value="${pub.title}"/><br/></p></h3></span></a>

                    </c:if>
                    <script>
                        $(function () {
                            $("h3").wrapInner("<span>");
                            $("h3 br").before("<span class='spacer'>")
                                .after("<span class='spacer'>");
                        });
                    </script>


                </div>
            </div>

            <div class="content">
                <div class="info">

                    <div class="date"><c:out value="${pub.simpleDate}"/></div>
                    <div class="stats">

                        </form>
                        <form class="comments" method="post" action="${pageContext.request.contextPath}/deletePost">
                            <input type="hidden" name="id" value="${pub.id}"/>
                            <input type="image" src="image/dump.png"style="width: 1.15rem;height: 1.15rem;">
                        </form>

                        <form class="likes" method="get" action="${pageContext.request.contextPath}/updatePost"
                              style="background-color: white;">
                            <input type="hidden" name="id" value="${pub.id}"/>
                            <input type="image" src="image/iconFix.png" style="width: 1.15rem;height: 1.15rem;">
                        </form>
                        <form class="likes" method="post" action="${pageContext.request.contextPath}/dash">
                            <input type="hidden" name="id" value="${pub.id}"/>
                            <input type="image" src="image/pub_icon.jpg" style="width: 1.15rem;height: 1.15rem;">

                        </form>
                    </div>
                </div>

                <div class="title">
                    <a href="/userPostPage?id=${pub.id}"><c:out value="${pub.description}"/></a>
                </div>
            </div>
        </article>



    </c:if>
</c:forEach>
</body>
</html>
