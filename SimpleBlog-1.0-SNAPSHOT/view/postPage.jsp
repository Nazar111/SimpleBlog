<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SimpleBlog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="layout/plugins/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="layout/style.css" type="text/css">
    <script type="text/javascript" src="layout/js/jquery.js"></script>
    <script type="text/javascript" src="layout/js/plugins.js"></script>
    <script type="text/javascript" src="layout/js/main.js"></script>
</head>
<body>
<c:if test="${logged==true}">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="btn btn-dark"  id="my-page" href="userPage.jsp">My page</a>

        <a class="btn btn-dark"  id="user-name-label" href="${pageContext.request.contextPath}/userPage.jsp"><c:out value="${sessionScope.name}"/></a>
        <details class="btn btn-dark btn-sm" >
            <summary>Change Data</summary>
            <form id="change-user-data" method="post" action="${pageContext.request.contextPath}/changeUserData">
                <input type="text" name="newName" placeholder="Enter new name">
                <input type="password" name="newPassword" placeholder="Enter new password">
                <input type="submit" value="Change"class="btn btn-success btn-sm" >
            </form>
        </details>
        <a class="btn btn-dark"  id="dash" href="${pageContext.request.contextPath}/dash">Dash</a>
        <a  class="btn btn-dark" href="${pageContext.request.contextPath}/logOut">Log out</a>
    </nav>
</c:if>
<c:if test="${logged==false}">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="btn btn-dark" href="${pageContext.request.contextPath}/registration" id="Registration" name="registration">Sign Up</a>

        <details class="btn btn-dark btn-sm">
            <summary>Log in</summary>

            <form id="login" method="post" action="${pageContext.request.contextPath}/enter">
                <input type="text" name="login" placeholder="Enter your login">
                <input type="password" name="password" placeholder="Enter your password"/>
                <input type="submit" value="Enter" class="btn btn-success btn-sm"/>
            </form>
            <p style="color: red"><c:out value="${message}"/></p>
        </details>

    </nav>
</c:if>
<div id="content" class="sidebar_right" style="margin-left: 16.5rem;">
    <div class="inner">

        <div class="block_general_title_2" style="margin-right: 11.81rem; ">
            <h1><c:out value="${title}"/></h1>
            <h2><p><span class="author">by  <c:out value="${name}"/></span>&nbsp;&nbsp;/&nbsp;&nbsp;<span
                    class="date"> <c:out value="${date}"/> </span></p></h2>

        </div>
        <div class="main_content" style="width: 47.244rem;">
            <div class="block_content">
                <div class="pic"><img src="content/${titleImage}" alt="" style="float: left;"></div>
                <h3><c:out value="${description}"/></h3>

                <p><c:out value="${text}"/> <br/></p>

                <div class="pic"><img src="content/${image}" alt=""></div>

                <div class="line_1"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
