<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SimpleBlog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="layout/plugins/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="layout/style.css" type="text/css">
    <script type="text/javascript" src="layout/js/jquery.js"></script>
    <script type="text/javascript" src="layout/js/plugins.js"></script>
    <script type="text/javascript" src="layout/js/main.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a href="${pageContext.request.contextPath}/userPage.jsp" class="btn btn-dark">My page</a>
</nav>
<div id="logo" style=""><a href="${pageContext.request.contextPath}/homePage"><img src="image/logo.png"></a></div>
<div class="container h-100" style="background-image: url(${pageContext.request.contextPath}/layout/images/fon_reg.jpg);">
    <div class="row h-100 justify-content-center align-items-center">
        <form id="add-post" method="post"  action="${pageContext.request.contextPath}/addPost" enctype="multipart/form-data" >
            <input type="text" name="title" placeholder="Title" class="form-control is-${invalidTitle}valid" required/>
            <c:forEach var="titleError" items="${titleError}"><p style="color: red"><c:out
                    value="${titleError.message}"/></p>
            </c:forEach>
            <input type="file" name="titleImage" class="form-control is-valid" />
            <textarea id="description" name="description" class="form-control is-${invalidDesc}valid"
                      placeholder="Add your description" cols="20" rows="7"
                      class="form-control" required></textarea>
            <c:forEach var="descriptionError" items="${descriptionError}"><p style="color: red">
                <c:out value="${descriptionError.message}"/></p></c:forEach>
            <textarea id="text" name="text" placeholder="Add your text" cols="50" rows="15"
                      class="form-control is-valid"></textarea>

            <input type="file" name="image" class="form-control" />
            <input type="submit" value="Publicate" class="btn btn-success btn-sm"/>
            <input type="submit" value="Add to dash" name="dash" class="btn btn-success btn-sm"/>


        </form>

    </div>
</div>
</body>
</html>
