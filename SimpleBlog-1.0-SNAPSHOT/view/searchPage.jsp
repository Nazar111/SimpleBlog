<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SimpleBlog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="layout/plugins/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="layout/style.css" type="text/css">
    <script type="text/javascript" src="layout/js/jquery.js"></script>
    <script type="text/javascript" src="layout/js/plugins.js"></script>
    <script type="text/javascript" src="layout/js/main.js"></script>
</head>
<body>
<c:if test="${logged==true}">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="btn btn-dark" id="my-page" href="userPage.jsp">My page</a>

        <a class="btn btn-dark" id="user-name-label" href="userPage.jsp"><c:out value="${sessionScope.name}"/></a>
        <details class="btn btn-dark btn-sm">
            <summary>Change Data</summary>
            <form id="change-user-data" method="post" action="${pageContext.request.contextPath}/changeUserData">
                <input type="text" name="newName" placeholder="Enter new name">
                <input type="password" name="newPassword" placeholder="Enter new password">
                <input type="submit" value="Change" class="btn btn-success btn-sm">
            </form>
        </details>
        <a class="btn btn-dark" id="dash" href="${pageContext.request.contextPath}/dash">Dash</a>
        <a class="btn btn-dark" href="/logOut">Log out</a>
    </nav>
    <div id="logo" style=""><a href="${pageContext.request.contextPath}/homePage"><img src="image/logo.png"></a></div>
</c:if>
<c:if test="${logged==false}">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="btn btn-dark" href="${pageContext.request.contextPath}/registration" id="Registration"
           name="registration">Sign Up</a>

        <details class="btn btn-dark btn-sm">
            <summary>Log in</summary>

            <form id="login" method="post" action="${pageContext.request.contextPath}/enter">
                <input type="text" name="login" placeholder="Enter your login">
                <input type="password" name="password" placeholder="Enter your password"/>
                <input type="submit" value="Enter" class="btn btn-success btn-sm"/>
            </form>
            <p style="color: red"><c:out value="${message}"/></p>
        </details>

    </nav>
    <div id="logo" style=""><img src="image/logo.png"></div>
</c:if>
<div class="block_general_title_1 w_margin_1">
    <h1>Search result</h1>
    <h3>found: <c:out value="${result}"/> posts</h3>
</div>
<div class="block_posts type_1 type_sort general_not_loaded">
    <div class="posts" style="margin-left: 5.5rem;">
        <c:forEach var="set" items="${set}" varStatus="status">
            <article class="post_type_6">
                <div class="feature">

                    <div class="image">
                        <a href="${pageContext.request.contextPath}/postPage?id=${set.id}"><img
                                src="content/${set.titleImage}" alt=""><span class="hover"></span></a>
                    </div>
                </div>

                <div class="content">
                    <div class="info" style="margin-top: 0.5rem">

                        <div class="author"><c:out value="${names[status.index]}"/></div>
                        <div class="date"><c:out value="${set.simpleDate}"/></div>
                    </div>

                    <div class="title">
                        <a href="${pageContext.request.contextPath}/postPage?id=${set.id}"><c:out
                                value="${set.title}"/></a>
                    </div>

                    <div class="text">
                        <p><c:out value="${set.description}"/></p>
                    </div>
                </div>
            </article>
        </c:forEach>

    </div>
</div>
</body>
</html>
