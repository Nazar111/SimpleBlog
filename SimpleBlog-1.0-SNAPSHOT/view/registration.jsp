<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SimpleBlog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style">

</head>
<body>
<div>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a href="${pageContext.request.contextPath}/" class="btn btn-dark">Home page</a>
    </nav>
    <div id="logo" style=""><a href="${pageContext.request.contextPath}"><img src="image/logo.png"></a></div>
    <div class="container h-100" style="background-image: url(${pageContext.request.contextPath}/layout/images/fon_reg.jpg);">
        <div class="row h-100 justify-content-center align-items-center">
            <fieldset>
                <legend>Enter your details</legend>
                <form id="RegistrationForm" method="post" action="${pageContext.request.contextPath}/authorization">
                    <div class="form-group  ">
                        <div class="">
                            <label for="validationServer01">Login</label>
                            <input type="text" class="form-control is-${invalidLogin}valid" id="validationServer01"
                                   placeholder="Login"
                                   name="login" required>
                            <div class="invalid-feedback">
                                <c:forEach var="errorLogin" items="${violationLogin}">
                                    <c:out value="${errorLogin.message}"/>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="">
                            <label for="validationServer01">Name</label>
                            <input type="text" class="form-control is-${invalidName}valid" id="validationServer02"
                                   placeholder="Name"
                                   name="name" required>
                            <div class="invalid-feedback">
                                <c:forEach var="errorName" items="${violationName}">
                                    <c:out value="${errorName.message}"/>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="">
                            <label for="validationServer01">Password</label>
                            <input type="text" class="form-control is-${invalidPassword}valid" id="validationServer03"
                                   placeholder="Password" name="password" required>
                            <div class="invalid-feedback">

                                <c:forEach var="errorPassword" items="${violationPassword}">
                                    <c:out value="${errorPassword.message}"/>
                                </c:forEach>
                            </div>
                        </div>
                        <label for="Role"><b>Admin</b></label>
                        <input type="checkbox" id="Role" name="Role"/>
                        <input type="submit" value="Registrate" class="btn btn-success btn-sm">
                        <p style="color: red"><c:out value="${message}"/></p>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>

</div>
</body>
</html>
